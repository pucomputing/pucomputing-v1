package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/pucomputing/pucomputing/interfaces"
	"gitlab.com/pucomputing/pucomputing/models"
)

type Messages struct {
	dbSession         *gorm.DB
	messagesInterface interfaces.Messages
}

func MessagesInit(db *gorm.DB) *Messages {
	return &Messages{
		dbSession: db,
	}
}

func (m Messages) MessagesSave(message models.Messages) error {
	if err := m.dbSession.Create(&message).Error; err != nil {
		return err
	}
	return nil
}

func (m Messages) MessagesRemove(message models.Messages) error {
	if err := m.dbSession.Delete(&message).Error; err != nil {
		return err
	}
	return nil
}
