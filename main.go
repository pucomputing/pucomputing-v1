package main

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitlab.com/pucomputing/pucomputing/controllers"
	"gitlab.com/pucomputing/pucomputing/db"
	"gitlab.com/pucomputing/pucomputing/models"

	echotemplate "github.com/foolin/echo-template"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// Starting datastore instance
	db := db.Init()
	// Migrating the models
	db.AutoMigrate(
		&models.Messages{},
	)

	// Starting Echo instance
	e := echo.New()
	// Setup the template renderer
	e.Renderer = echotemplate.Default()

	// Middleware
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${level}[${method}] ${uri} ${status}\n",
	}))
	// e.Use(middleware.Logger())
	// e.Use(middleware.Recover())

	// Static files
	e.Static("/static", "static")

	controller := controllers.Init(db)

	// Routes
	e.GET("/", controller.HomeShow)
	e.GET("/about", controller.HomeShow)
	e.GET("/contact", controller.HomeShow)
	e.POST("/api/message/send", controller.MessagesCreate)

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}
