package controllers

import (
	"net/http"

	"gitlab.com/pucomputing/pucomputing/models"

	"github.com/labstack/echo"
)

func (h Handler) MessagesCreate(c echo.Context) (err error) {
	type FormMessage struct {
		Name    string `form:"name"`
		Email   string `form:"email"`
		Message string `form:"message"`
	}

	var response = struct {
		Success bool `json:"success"`
	}{
		true,
	}

	formMessage := new(FormMessage)
	if err = c.Bind(formMessage); err != nil {
		response.Success = false
		return c.JSON(http.StatusOK, &response)
	}

	message := models.Messages{
		Name:    formMessage.Name,
		Email:   formMessage.Email,
		Message: formMessage.Message,
	}

	if err = h.messageRepository.MessagesSave(message); err != nil {
		response.Success = false
		return c.JSON(http.StatusOK, &response)
	}

	return c.JSON(http.StatusOK, &response)
}
