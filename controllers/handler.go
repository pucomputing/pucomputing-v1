package controllers

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/pucomputing/pucomputing/repository"
)

type Handler struct {
	messageRepository *repository.Messages
}

func Init(dbSession *gorm.DB) Handler {
	return Handler{
		messageRepository: repository.MessagesInit(dbSession),
	}
}
