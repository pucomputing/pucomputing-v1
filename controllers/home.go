package controllers

import (
	"net/http"

	"github.com/labstack/echo"
)

func (h Handler) HomeShow(c echo.Context) error {
	return c.Render(http.StatusOK, "index.html", echo.Map{})
}
