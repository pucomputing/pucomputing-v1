build: build-ui build-backend

build-ui: clean setup-build-folder
	bash -c "cd ui/ && npm run build && mv build/index.html ../build/views/index.html && mv build/static ../build && rm -rf build/*"

build-backend:
	bash -c "go build main.go && mv main build/main"

setup-build-folder:
	bash -c "mkdir build && mkdir build/views && mkdir build/static"

clean:
	bash -c "rm -rf build"

run-dev:
	bash -c "source development.env && go run main.go"

test-dev:
	bash -c "source development.env && go test -v -cover -covermode=atomic ./..."