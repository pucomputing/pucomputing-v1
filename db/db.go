package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"

	// Database drivers
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Init used to initialize the database
func Init() *gorm.DB {
	// https://gorm.io/docs/connecting_to_the_database.html
	databaseHost := os.Getenv("databaseHost")
	databasePort := os.Getenv("databasePort")
	databaseName := os.Getenv("databaseName")
	databaseUser := os.Getenv("databaseUser")
	databasePassword := os.Getenv("databasePassword")
	databaseType := os.Getenv("databaseType")

	var databaseURL string

	switch databaseType {
	case "postgres":
		databaseURL = fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", databaseHost, databasePort, databaseUser, databaseName, databasePassword)
	case "mysql":
		databaseURL = fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", databaseUser, databasePassword, databaseName)
	default:
		panic(fmt.Sprintf("%s is not a valid database type", databaseType))
	}

	db, err := gorm.Open(databaseType, databaseURL)
	if err != nil {
		panic(fmt.Sprintf("Failed to connect database\nError: %s", err))
	}

	return db
}
