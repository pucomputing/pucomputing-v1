# PUComputing

President University Major Asociation for Faculty of Computer Science website that build on top of [ReactJS](https://reactjs.org/) for its interface and [Echo](https://echo.labstack.com/) for its backend.

## Requirement

- [NodeJS](https://nodejs.org/)
- [npm](https://www.npmjs.com/)
- [Golang](https://golang.org/)

## Building the website

For the first time, you run the command below on `interface/` directory

```
npm install
```

Then, you can build the website using these command

```
make
```

The builded website are available on `build/` directory

## LICENSE

The website itself is MIT licensed, except for the static folder is not for public use.