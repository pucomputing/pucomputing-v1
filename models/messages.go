package models

import "github.com/jinzhu/gorm"

// Messages model
type Messages struct {
	gorm.Model
	ID      uint `gorm:"primary_key"`
	Name    string
	Email   string
	Message string
}
