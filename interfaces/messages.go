package interfaces

import (
	"gitlab.com/pucomputing/pucomputing/models"
)

type Messages interface {
	Save(models.Messages) error
	FindById(int) (models.Messages, error)
	LookUpPage(int, int) ([]models.Messages, error)
	Remove(int) error
}
